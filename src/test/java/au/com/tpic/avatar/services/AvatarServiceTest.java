package au.com.tpic.avatar.services;

import au.com.tpic.avatar.configuration.AvatarServiceProperties;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;


class AvatarServiceTest {

    private final AvatarService avatarService;

    public AvatarServiceTest() throws IOException {
        AvatarServiceProperties properties = new AvatarServiceProperties();
        properties.setDirectory(Path.of("src", "test", "resources"));
        this.avatarService = new AvatarService(properties);
    }

    @Test
    void getDefaultAvatar() throws IOException {
        byte[] expectedBody = getClass().getResourceAsStream("/default-200.jpg").readAllBytes();
        ResponseEntity<byte[]> expectedResponse = ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(expectedBody);
        // Files.write(avatarService.getAvatar(null, "200").getBody(),new File("default-200.jpg"));
        assertThat(avatarService.getAvatar(null, "200")).isEqualTo(expectedResponse);
    }

    @Test
    void getIdenticonAvatar() throws IOException {
        byte[] expectedBody = getClass().getResourceAsStream("/identicon-200.jpg").readAllBytes();
        ResponseEntity<byte[]> expectedResponse = ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(expectedBody);
        assertThat(avatarService.getAvatar("B725DDDD5F2F979522DCC55A05E", "200")).isEqualTo(expectedResponse);
    }

    @Test
    void getAvatarFromDirectory() throws IOException {
        byte[] expectedBody = getClass().getResourceAsStream("/test-200.png").readAllBytes();
        ResponseEntity<byte[]> expectedResponse = ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(expectedBody);
        assertThat(avatarService.getAvatar("098f6bcd4621d373cade4e832627b4f6.png", "200")).isEqualTo(expectedResponse);
    }

    @Test
    void getResizedAvatarFromDirectory() throws IOException {
        byte[] expectedBody = getClass().getResourceAsStream("/test-100.jpg").readAllBytes();
        ResponseEntity<byte[]> expectedResponse = ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(expectedBody);
        assertThat(avatarService.getAvatar("098f6bcd4621d373cade4e832627b4f6", "100")).isEqualTo(expectedResponse);
    }

}