package au.com.tpic.avatar.web;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ActiveProfiles("unit-test")
@AutoConfigureMockMvc
class AvatarControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @ParameterizedTest
    @CsvSource({
            "/avatar,/default-200.jpg,image/jpeg", // getDefaultAvatar
            "/avatar/098f6bcd4621d373cade4e832627b4f6.png,/test-200.png,image/png", // getAvatarFromDirectory
            "/avatar/098f6bcd4621d373cade4e832627b4f6?s=100,/test-100.jpg,image/jpeg" // getResizedAvatarFromDirectory
    })
    void success(String urlTemplate, String imagePath, String mediaType) throws Exception {
        byte[] expectedImage = loadImage(imagePath);
        mockMvc.perform(get(urlTemplate))
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaType))
                .andExpect(content().bytes(expectedImage));
    }

    @Test
    void requestValidation() throws Exception {
        mockMvc.perform(get("/avatar/ "))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.path").value("/avatar/%20"))
                .andExpect(jsonPath("$.message").value("Request should not be blank"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"a", "0", "-100", "1", "a34", "4097", "10000"})
    void sizeValidation(String size) throws Exception {
        String template = String.format("/avatar?s=%s", size);
        mockMvc.perform(get(template))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.path").value("/avatar"))
                .andExpect(jsonPath("$.message").value("Avatar size be a valid number between 3 and 4096"));
    }

    private byte[] loadImage(String name) {
        try (InputStream stream = getClass().getResourceAsStream(name)) {
            assertThat(stream).isNotNull();
            return stream.readAllBytes();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}