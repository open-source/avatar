package au.com.tpic.avatar.validator;

import com.google.common.base.Strings;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class NumericValueValidator implements ConstraintValidator<NumericValue, String> {

    private boolean mandatory;
    private int minValue;
    private int maxValue;

    @Override
    public void initialize(NumericValue constraintAnnotation) {
        this.mandatory = constraintAnnotation.mandatory();
        this.minValue = constraintAnnotation.minValue();
        this.maxValue = constraintAnnotation.maxValue();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (Strings.nullToEmpty(value).isEmpty()) {
            return !mandatory;
        }

        boolean valid = true;
        try {
            int i = Integer.parseInt(value);
            if (i < minValue || i > maxValue) {
                valid = false;
            }
        } catch (NumberFormatException e) {
            valid = false;
        }
        return valid;
    }

}
