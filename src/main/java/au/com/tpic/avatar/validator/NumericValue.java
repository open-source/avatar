package au.com.tpic.avatar.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Retention(RUNTIME)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Constraint(validatedBy = {NumericValueValidator.class})
public @interface NumericValue {

    String message() default "Must be a valid number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int minValue() default Integer.MIN_VALUE;

    int maxValue() default Integer.MAX_VALUE;

    boolean mandatory() default false;

}
