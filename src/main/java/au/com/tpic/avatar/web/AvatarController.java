package au.com.tpic.avatar.web;

import au.com.tpic.avatar.services.AvatarService;
import au.com.tpic.avatar.validator.NumericValue;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import jakarta.validation.constraints.NotBlank;

@Validated
@RestController
@AllArgsConstructor
@RequestMapping("/avatar")
public class AvatarController {

    private final Logger log = LoggerFactory.getLogger(AvatarController.class);
    private final AvatarService service;

    @GetMapping
    public ResponseEntity<byte[]> getDefaultAvatar(@NumericValue(message = "Avatar size be a valid number between 3 and 4096",
            minValue = 3,
            maxValue = 4096) @RequestParam(value = "s", required = false, defaultValue = "200") String size) throws IOException {
        log.debug("Getting default avatar [size={}]", size);
        return service.getAvatar(null, size);
    }

    @GetMapping(value = "{request}")
    public ResponseEntity<byte[]> getAvatar(@NotBlank(message = "Request should not be blank") @PathVariable String request,
                                            @NumericValue(message = "Avatar size be a valid number between 3 and 4096",
                                                    minValue = 3,
                                                    maxValue = 4096) @RequestParam(value = "s",
                                                    required = false,
                                                    defaultValue = "200") String size) throws IOException {
        log.debug("Getting avatar [request={}, size={}]", request, size);
        return service.getAvatar(request, size);
    }
}
