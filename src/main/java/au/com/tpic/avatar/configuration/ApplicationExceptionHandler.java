package au.com.tpic.avatar.configuration;

import au.com.tpic.avatar.model.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;

@RestControllerAdvice
public class ApplicationExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(ApplicationExceptionHandler.class);
    private static final String DEFAULT_MESSAGE = "An unknown error occurred";

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleConstraintViolationException(HttpServletRequest request, ConstraintViolationException e) {
        String firstValidationError = e.getConstraintViolations().stream().findFirst().orElseThrow().getMessage();
        ErrorResponse response = ErrorResponse.builder()
                .status(HttpStatus.BAD_REQUEST)
                .path(request.getRequestURI())
                .message(firstValidationError)
                .build();
        log.error("Constraint validation error [response={}]", response, e);
        return response;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleException(HttpServletRequest request, Exception e) {
        ErrorResponse response = ErrorResponse.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .path(request.getRequestURI())
                .message(DEFAULT_MESSAGE)
                .build();
        log.error(DEFAULT_MESSAGE + " [response={}]", response, e);
        return response;
    }


}
