package au.com.tpic.avatar.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;

@Configuration
@ConfigurationProperties(value = "avatar")
@Data
public class AvatarServiceProperties {

    private Path directory;

}
