package au.com.tpic.avatar.services;

import au.com.tpic.avatar.configuration.AvatarServiceProperties;
import com.google.common.base.Preconditions;
import com.mortennobel.imagescaling.ResampleFilters;
import com.mortennobel.imagescaling.ResampleOp;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;
import javax.imageio.ImageIO;

@Service
public class AvatarService {

    private final HashMap<String, Path> fileList;
    private final BufferedImage defaultImage;
    private static final String DEFAULT_FORMAT = "jpg";

    public AvatarService(AvatarServiceProperties properties) throws IOException {
        this.fileList = new HashMap<>();
        try (Stream<Path> avatars = Files.walk(properties.getDirectory());
             InputStream dis = getClass().getResourceAsStream("/default.jpg")) {
            Preconditions.checkNotNull(dis, "Default image not found");
            avatars.filter(this::correctFormat)
                    .forEach(path -> this.fileList.put(getFileHash(path), path));
            this.defaultImage = ImageIO.read(dis);
        }
    }

    public ResponseEntity<byte[]> getAvatar(String request, String size) throws IOException {
        try {
            String hash = getRequestHash(request);
            MediaType format = getRequestFormat(request);
            BufferedImage image = (fileList.containsKey(hash)) ? readImageToSize(fileList.get(hash), Integer.parseInt(size)) : generateIdenticons(hash, Integer.parseInt(size));
            return ResponseEntity.ok().contentType(format).body(writeToFormat(image, format.getSubtype()));
        } catch (Exception ignored) {
            BufferedImage resizedDefaultImage = resizeImage(defaultImage, Integer.parseInt(size));
            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(writeToFormat(resizedDefaultImage, DEFAULT_FORMAT));
        }
    }

    private static BufferedImage generateIdenticons(String text, int size) {
        int width = 5;
        int height = 5;

        byte[] hash = text.getBytes();

        BufferedImage identicon = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        WritableRaster raster = identicon.getRaster();

        int[] background = new int[]{255, 255, 255};
        int[] foreground = new int[]{hash[0] & 255, hash[hash.length / 3] & 255, hash[hash.length / 2] & 255};

        for (int x = 0; x < width; x++) {
            //Enforce horizontal symmetry
            int i = x < 3 ? x : 4 - x;
            for (int y = 0; y < height; y++) {
                int[] pixelColor;
                //toggle pixels based on bit being on/off
                if ((hash[i] >> y & 1) == 1)
                    pixelColor = foreground;
                else
                    pixelColor = background;
                raster.setPixel(x, y, pixelColor);
            }
        }

        //Scale image to the size you want
        AffineTransform at = new AffineTransform();
        at.scale((double) size / width, (double) size / height);
        AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        return op.filter(identicon, null);
    }

    private BufferedImage readImageToSize(Path imagePath, int size) throws IOException {
        BufferedImage inputImage = ImageIO.read(imagePath.toFile());
        return resizeImage(inputImage, size);
    }

    private BufferedImage resizeImage(BufferedImage inputImage, int size) {
        int newHeight = inputImage.getHeight() / inputImage.getWidth() * size;
        ResampleOp resizeOp = new ResampleOp(size, newHeight);
        resizeOp.setFilter(ResampleFilters.getLanczos3Filter());
        return resizeOp.filter(inputImage, null);
    }

    private byte[] writeToFormat(BufferedImage image, String format) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (List.of("jpg", "jpeg", "jfif").contains(format)) {
            // Remove alpha channel
            BufferedImage result = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
            result.createGraphics().drawImage(image, 0, 0, Color.BLACK, null);
            image = result;
        }
        ImageIO.write(image, format, baos);
        return baos.toByteArray();
    }

    private String getFileHash(Path path) {
        String filename = com.google.common.io.Files.getNameWithoutExtension(path.getFileName().toString());
        return DigestUtils.md5DigestAsHex(filename.getBytes());
    }

    private boolean correctFormat(Path path) {
        List<String> accepted = List.of("jpg", "jpeg", "jfif", "png", "gif");
        String extension = com.google.common.io.Files.getFileExtension(path.getFileName().toString());
        return accepted.contains(extension.toLowerCase());
    }

    private String getRequestHash(String request) {
        return request.toLowerCase().split("\\.")[0];
    }

    private MediaType getRequestFormat(String request) {
        try {
            String format = request.toLowerCase().split("\\.")[1];
            return MediaType.valueOf("image/" + format);
        } catch (Exception ignored) {
            return MediaType.IMAGE_JPEG;
        }
    }

}
